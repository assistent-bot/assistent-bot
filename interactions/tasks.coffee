mkId = ()=> Math.random().toString(36).split('.')[1]

module.exports = tasks = (bot, update)->
  chatId = update._act?.chat?.id
  msgId = update._act?.message_id or 0
  msg = update._act?.text or ''
  waitingAnswer = waitingAnswers[chatId]
  if waitingAnswer?.kind is 'SET_STEPS'
    # TODO: make it cancelable. Must also delete the task.
    steps = msg.trim()[0].match /^([0-9]+)\s*(steps?)\.?$/i
    if steps
      task = memory[chatId][waitingAnswer.taskId]
      task.steps = parseInt(steps[1])
      bot.sendMessage "Task #{taskId} \"#{task.title}\" saved!", chatId, reply_to_message_id: msgId
      delete waitingAnswers[chatId]
    else
      bot.sendMessage "You must send only a number, to set how many steps for this task.", chatId, reply_to_message_id: msgId
  else if msg.trim()[0] is '/'
    '''
    `/newtask <type> <title>`
    `/list` (display tasks and ids)
    `/done <id>`
    `/export
    '''
    cmd = msg.trim().match(/^\/([a-z_0-9]+)/)[1]
    if commands[cmd]
      commands[cmd] bot, update
    else
      bot.sendMessage "Unrecognized command #{cmd}", chatId, reply_to_message_id: msgId

# TODO: makes `memory` and `waitingAnswers` instances of a class with persistence hability. All changes must be done by this class methods.
memory = {}
waitingAnswers = {}

commands =
  newtask: (bot, update)->
    chatId = update._act?.chat?.id
    msgId = update._act?.message_id or 0
    cmd = update._act.text.trim().match(/^\/([a-z_0-9]+)\s+([a-z]+)\s+(.*)/)
    chatMemo = memory[chatId] ?= {}
    taskId = mkId() while !taskId and chatMemo[taskId]
    taskType = cmd[2].toUpperCase()
    isSingleTask = taskType is 'SINGLE'
    task =
      id: taskId
      title: cmd[3]
      type: taskType
      createDate: new Date
      updateDate: new Date
      startDate: new Date
      deadline: null
      priority: 5
      steps: if isSingleTask then 1 else null
      walkedSteps: 0
      done: false
    chatMemo[taskId] = task
    if isSingleTask
      bot.sendMessage "Task #{taskId} \"#{task.title}\" saved!", chatId, reply_to_message_id: msgId
    else
      bot.sendMessage "How many steps for this task?", chatId, reply_to_message_id: msgId
      waitingAnswers[chatId] =
        kind: 'SET_STEPS'
        taskId: taskId
        origMsgId = msgId
