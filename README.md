Bot Projetos
============

A Telegram assistant bot for life goals and projects.

Main features
-------------

* Task manager
  - Task types:
    + Countable Task
    + Single Task
  - Start Date
  - Deadline

Commands
--------
* `/newtask <type> <title>`
* `/list` (display tasks and ids)
* `/done <id>`
* `/export` (return todo.txt http://todotxt.org/ or custom json data)
